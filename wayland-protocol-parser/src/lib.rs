// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ---------------------------------------------------------
//
// # Lint Levels
//
// Lints are divided into four levels:
//
// 1. allow
// 2. warn
// 3. deny
// 4. forbid
//
// To override the lints set here, use:
//
// ```sh
// $ RUSTFLAGS="--cap-lints warn" cargo build
// ```
//
// ---------------------------------------------------------
//
// # Clippy lints
//
// code that is just outright wrong or very very useless
#![forbid(clippy::correctness)]
// code that should be written in a more idiomatic way
#![forbid(clippy::style)]
// code that does something simple but in a complex way
#![forbid(clippy::complexity)]
// code that can be written in a faster way
#![forbid(clippy::perf)]
// lints which are rather strict
#![forbid(clippy::pedantic)]
// new lints that aren't quite ready yet
#![forbid(clippy::nursery)]
// checks against the cargo manifest
#![forbid(clippy::cargo)]
// ---------------------------------------------------------
//
// # Lint Groups
//
// Lint groups toggle several lints through one name.
// The following lint groups are available:
//
// - nonstandard-style
// - warnings
// - rust-2018-idioms
// - unused
// - future-incompatible
//
// Violation of standard naming conventions
#![forbid(nonstandard_style)]
// all lints that would be issuing warnings
#![forbid(warnings)]
// Lints to nudge you toward idiomatic features of Rust 2018
#![forbid(rust_2018_idioms)]
// These lints detect things being declared but not used
#![forbid(unused)]
// Lints that detect code that has future-compatibility problems
#![forbid(future_incompatible)]
// ---------------------------------------------------------
//
// # Other lints
//
// This lint detects the use of hidden lifetime parameters.
#![forbid(elided_lifetimes_in_paths)]
// This lint detects potentially-forgotten implementations of Copy.
#![forbid(missing_copy_implementations)]
// This lint detects missing implementations of fmt::Debug.
#![forbid(missing_debug_implementations)]
// This lint detects missing documentation for public items.
#![forbid(missing_docs)]
// This lint detects lifetimes that are only used once.
#![forbid(single_use_lifetimes)]
// This lint detects trivial casts which could be removed.
#![forbid(trivial_casts)]
// This lint detects trivial casts of numeric types which could be removed.
#![forbid(trivial_numeric_casts)]
// This lint catches usage of unsafe code.
#![forbid(unsafe_code)]
// This lint catches unnecessary braces around an imported item.
#![forbid(unused_import_braces)]
// This lint detects unnecessarily qualified names.
#![forbid(unused_qualifications)]
// This lint checks for the unused result of an expression in a statement.
#![forbid(unused_results)]
// This lint detects enums with widely varying variant sizes.
#![forbid(variant_size_differences)]
// ---------------------------------------------------------
//
// # Lint overrides

//! # Wayland Protocol Parser
//!
//! wayland-protocol-parser is a library for converting Wayland XML Protocol
//! specification into an Abstract Syntax Tree.
//!
//! ## Usage
//!
//! ```rust
//! use vlog::Context;
//! use std::{
//!     fs::read_to_string,
//!     path::Path,
//! };
//! use wayland_protocol_parser::parse;
//!
//! let context = Context::default();
//!
//! let path = Path::new("protocol/wayland.xml");
//! let contents =
//!     read_to_string(path).expect("failed to read wayland.xml file");
//!
//! let output = parse(context, &contents);
//! ```

// Do not include standard library
#![no_std]
// Allow configuration for documentation
#![feature(doc_cfg)]

extern crate alloc;

// Allow using standard library for documentation tests
#[doc(cfg(test))]
extern crate std;

use alloc::{
    format,
    string::String,
    string::ToString,
    vec::Vec,
};
use vlog::{
    event,
    Context,
    Level,
    Sink,
};
use roxmltree::{
    Document,
    Node,
};
use wayland_protocol::{
    Argument,
    ArgumentName,
    ArgumentType,
    Copyright,
    Entry,
    EntryName,
    EntryValue,
    Enumeration,
    EnumerationName,
    Interface,
    InterfaceName,
    InterfaceVersion,
    Message,
    MessageName,
    MessageType,
    Protocol,
    ProtocolName,
};

/// Generate rust code from the Wayland XML Protocol specification.
#[inline]
pub fn parse<S: Sink>(
    context: Context<S>,
    contents: &str,
) -> Protocol {
    // Create a child context
    let child_context = context.child(String::from("wayland-protocol-parser"));

    event!(&child_context, Level::Trace, message = "BEGIN: parse");
    let document =
        Document::parse(contents).expect("failed to parse wayland.xml file");
    let output = parse_protocol(&child_context, document.root_element());
    event!(&child_context, Level::Trace, message = "END: parse");
    output
}

/// Parse the `protocol` tag in the XML document.
#[inline]
fn parse_protocol<'input, S: Sink>(
    context: &Context<S>,
    node: Node<'input, 'input>,
) -> Protocol {
    event!(context, Level::Trace, message = "BEGIN parse_protocol");

    assert!(node.has_tag_name("protocol"));

    let mut protocol = Protocol::default();

    assert!(node.has_attribute("name"));
    protocol.name = ProtocolName(node.attribute("name").unwrap().to_string());

    for child in node.children() {
        match child.tag_name().name() {
            "" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: empty"
                );
            },
            "copyright" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: copyright"
                );
                let mut copyright = String::default();
                for line in child.text().unwrap().lines() {
                    copyright.push_str(line.trim());
                    copyright.push('\n');
                }
                protocol.copyright =
                    Some(Copyright(copyright));
            },
            "interface" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: interface"
                );
                protocol.interfaces.push(parse_interface(context, child));
            },
            unknown => {
                event!(
                    context,
                    Level::Fatal,
                    message = &format!("matched child node: {}", unknown)
                );
                panic!("unknown child node: {}", unknown);
            },
        }
    }

    event!(context, Level::Trace, message = "END parse_protocol");

    protocol
}

/// Parse the `interface` tag in the XML document.
#[inline]
fn parse_interface<'input, S: Sink>(
    context: &Context<S>,
    node: Node<'input, 'input>,
) -> Interface {
    event!(context, Level::Trace, message = "BEGIN parse_interface");

    assert!(node.has_tag_name("interface"));

    let mut interface = Interface::default();

    assert!(node.has_attribute("name"));
    interface.name = InterfaceName(node.attribute("name").unwrap().to_string());

    assert!(node.has_attribute("version"));
    interface.version =
        InterfaceVersion(node.attribute("version").unwrap().parse().unwrap());

    for child in node.children() {
        match child.tag_name().name() {
            "" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: empty"
                );
            },
            "description" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: description"
                );
                interface.description = child.text().map(str::to_string);
                interface.summary =
                    child.attribute("summary").map(str::to_string);
            },
            "request" | "event" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: message"
                );
                interface.messages.push(parse_message(context, child));
            },
            "enum" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: enum"
                );
                interface.enumerations.push(parse_enumeration(context, child));
            },
            unknown => {
                event!(
                    context,
                    Level::Fatal,
                    message = &format!("matched child node: {}", unknown)
                );
                panic!("unknown child node: {}", unknown);
            },
        }
    }

    event!(context, Level::Trace, message = "END parse_interface");

    interface
}

/// Parse either the `request` tag or the `event` tag in the XML document.
#[inline]
fn parse_message<'input, S: Sink>(
    context: &Context<S>,
    node: Node<'input, 'input>,
) -> Message {
    event!(context, Level::Trace, message = "BEGIN parse_request");

    let r#type = match node.tag_name().name() {
        "request" => MessageType::Request,
        "event" => MessageType::Event,
        _ => {
            event!(context, Level::Fatal, message = "unknown message type");
            panic!("unknown message type")
        },
    };

    assert!(node.has_attribute("name"));
    let name = MessageName(node.attribute("name").unwrap().to_string());

    let mut message = Message {
        name,
        description: None,
        summary: None,
        r#type,
        arguments: Vec::default(),
    };

    for child in node.children() {
        match child.tag_name().name() {
            "" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: empty"
                );
            },
            "description" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: description"
                );
                message.description = child.text().map(str::to_string);
                message.summary =
                    child.attribute("summary").map(str::to_string);
            },
            "arg" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: arg"
                );
                message.arguments.push(parse_argument(context, child));
            },
            unknown => {
                event!(
                    context,
                    Level::Fatal,
                    message = &format!("matched child node: {}", unknown)
                );
                panic!("unknown child node: {}", unknown);
            },
        }
    }

    event!(context, Level::Trace, message = "END parse_request");

    message
}

/// Parse the `arg` tag in the XML document.
#[inline]
fn parse_argument<'input, S: Sink>(
    context: &Context<S>,
    node: Node<'input, 'input>,
) -> Argument {
    event!(context, Level::Trace, message = "BEGIN parse_argument");

    assert!(node.has_tag_name("arg"));

    assert!(node.has_attribute("name"));
    let name = ArgumentName(node.attribute("name").unwrap().to_string());

    assert!(node.has_attribute("type"));
    let r#type = parse_type(context, node.attribute("type").unwrap());

    let interface =
        node.attribute("interface").map(|x| InterfaceName(x.to_string()));

    let description = node.attribute("description").map(str::to_string);
    let summary = node.attribute("summary").map(str::to_string);

    let mut allow_null = false;
    if let Some("true") = node.attribute("allow-null") {
        allow_null = true;
    }

    event!(context, Level::Trace, message = "END parse_argument");

    Argument {
        name,
        r#type,
        interface,
        description,
        summary,
        allow_null,
    }
}

/// Parse a type in the XML document.
#[inline]
fn parse_type<S: Sink>(
    context: &Context<S>,
    value: &str,
) -> ArgumentType {
    match value {
        "int" => ArgumentType::SignedInteger,
        "uint" => ArgumentType::UnsignedInteger,
        "fixed" => ArgumentType::FixedPoint,
        "string" => ArgumentType::String,
        "object" => ArgumentType::Object,
        "new_id" => ArgumentType::NewId,
        "array" => ArgumentType::Array,
        "fd" => ArgumentType::FileDescriptor,
        unknown => {
            event!(
                context,
                Level::Fatal,
                message = &format!("unknown type: {}", unknown)
            );
            panic!("Unknown type: {}", unknown)
        },
    }
}

/// Parse the `enum` tag in the XML document.
#[inline]
fn parse_enumeration<'input, S: Sink>(
    context: &Context<S>,
    node: Node<'input, 'input>,
) -> Enumeration {
    event!(context, Level::Trace, message = "BEGIN parse_enumeration");

    assert!(node.has_tag_name("enum"));

    let mut enumeration = Enumeration::default();

    assert!(node.has_attribute("name"));
    enumeration.name =
        EnumerationName(node.attribute("name").unwrap().to_string());

    if node.has_attribute("bitfield") {
        enumeration.bitfield =
            node.attribute("bitfield").unwrap().parse().unwrap();
    }

    for child in node.children() {
        match child.tag_name().name() {
            "" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: empty"
                );
            },
            "description" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: description"
                );
                enumeration.description = child.text().map(str::to_string);
                enumeration.summary =
                    child.attribute("summary").map(str::to_string);
            },
            "entry" => {
                event!(
                    context,
                    Level::Trace,
                    message = "matched child node: entry"
                );
                enumeration.entries.push(parse_entry(
                    context,
                    child,
                    enumeration.bitfield,
                ));
            },
            unknown => {
                event!(
                    context,
                    Level::Fatal,
                    message = &format!("matched child node: {}", unknown)
                );
                panic!("unknown child node: {}", unknown);
            },
        }
    }

    event!(context, Level::Trace, message = "END parse_enumeration");

    enumeration
}

/// Parse the `entry` tag in the XML document.
#[inline]
fn parse_entry<'input, S: Sink>(
    context: &Context<S>,
    node: Node<'input, 'input>,
    bitfield: bool,
) -> Entry {
    event!(context, Level::Trace, message = "BEGIN parse_entry");

    assert!(node.has_tag_name("entry"));

    assert!(node.has_attribute("name"));
    let name = EntryName(node.attribute("name").unwrap().to_string());

    let description = node.attribute("description").map(str::to_string);
    let summary = node.attribute("summary").map(str::to_string);

    assert!(node.has_attribute("value"));
    let value = if bitfield {
        let number = node.attribute("value").unwrap();
        if number.starts_with("0x") {
            EntryValue::BitfieldValue(
                usize::from_str_radix(
                    node.attribute("value").unwrap().trim_start_matches("0x"),
                    16,
                )
                .unwrap(),
            )
        } else if number.starts_with("0b") {
            EntryValue::BitfieldValue(
                usize::from_str_radix(
                    node.attribute("value").unwrap().trim_start_matches("0b"),
                    2,
                )
                .unwrap(),
            )
        } else {
            EntryValue::BitfieldValue(
                usize::from_str_radix(node.attribute("value").unwrap(), 10)
                    .unwrap(),
            )
        }
    } else {
        let number = node.attribute("value").unwrap();
        if number.starts_with("0x") {
            EntryValue::EnumerantValue(
                isize::from_str_radix(
                    node.attribute("value").unwrap().trim_start_matches("0x"),
                    16,
                )
                .unwrap(),
            )
        } else if number.starts_with("0b") {
            EntryValue::EnumerantValue(
                isize::from_str_radix(
                    node.attribute("value").unwrap().trim_start_matches("0b"),
                    2,
                )
                .unwrap(),
            )
        } else {
            EntryValue::EnumerantValue(
                isize::from_str_radix(node.attribute("value").unwrap(), 10)
                    .unwrap(),
            )
        }
    };

    event!(context, Level::Trace, message = "END parse_entry");

    Entry {
        name,
        description,
        summary,
        value,
    }
}
