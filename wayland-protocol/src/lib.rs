// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ---------------------------------------------------------
//
// # Lint Levels
//
// Lints are divided into four levels:
//
// 1. allow
// 2. warn
// 3. deny
// 4. forbid
//
// To override the lints set here, use:
//
// ```sh
// $ RUSTFLAGS="--cap-lints warn" cargo build
// ```
//
// ---------------------------------------------------------
//
// # Clippy lints
//
// code that is just outright wrong or very very useless
#![forbid(clippy::correctness)]
// code that should be written in a more idiomatic way
#![forbid(clippy::style)]
// code that does something simple but in a complex way
#![forbid(clippy::complexity)]
// code that can be written in a faster way
#![forbid(clippy::perf)]
// lints which are rather strict
#![forbid(clippy::pedantic)]
// new lints that aren't quite ready yet
#![forbid(clippy::nursery)]
// checks against the cargo manifest
#![forbid(clippy::cargo)]
// ---------------------------------------------------------
//
// # Lint Groups
//
// Lint groups toggle several lints through one name.
// The following lint groups are available:
//
// - nonstandard-style
// - warnings
// - rust-2018-idioms
// - unused
// - future-incompatible
//
// Violation of standard naming conventions
#![forbid(nonstandard_style)]
// all lints that would be issuing warnings
#![forbid(warnings)]
// Lints to nudge you toward idiomatic features of Rust 2018
#![forbid(rust_2018_idioms)]
// These lints detect things being declared but not used
#![forbid(unused)]
// Lints that detect code that has future-compatibility problems
#![forbid(future_incompatible)]
// ---------------------------------------------------------
//
// # Other lints
//
// This lint detects the use of hidden lifetime parameters.
#![forbid(elided_lifetimes_in_paths)]
// This lint detects potentially-forgotten implementations of Copy.
#![forbid(missing_copy_implementations)]
// This lint detects missing implementations of fmt::Debug.
#![forbid(missing_debug_implementations)]
// This lint detects missing documentation for public items.
#![forbid(missing_docs)]
// This lint detects lifetimes that are only used once.
#![forbid(single_use_lifetimes)]
// This lint detects trivial casts which could be removed.
#![forbid(trivial_casts)]
// This lint detects trivial casts of numeric types which could be removed.
#![forbid(trivial_numeric_casts)]
// This lint catches usage of unsafe code.
#![forbid(unsafe_code)]
// This lint catches unnecessary braces around an imported item.
#![forbid(unused_import_braces)]
// This lint detects unnecessarily qualified names.
#![forbid(unused_qualifications)]
// This lint checks for the unused result of an expression in a statement.
#![forbid(unused_results)]
// This lint detects enums with widely varying variant sizes.
#![forbid(variant_size_differences)]
// ---------------------------------------------------------
//
// # Lint overrides
//
// Needed for deriving traits
#![deny(unused_qualifications)]

//! # Wayland Protocol
//!
//! Representation of the Wayland XML Protocol specification.

// Do not use the standard library
#![no_std]
// Enable configuration for documentation
#![feature(doc_cfg)]

extern crate alloc;

// XXX: See https://github.com/rust-lang/rust/issues/54010
#[doc(cfg(test))]
extern crate std;

use alloc::{
    string::String,
    vec::Vec,
};
use core::fmt;

/// Name of the `Protocol`
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct ProtocolName(pub String);

impl fmt::Display for ProtocolName {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Copyright notice.
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct Copyright(pub String);

impl fmt::Display for Copyright {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Name of an `Interface`
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct InterfaceName(pub String);

impl fmt::Display for InterfaceName {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// # Interface Version
///
/// Every `Interface` is versioned and every protocol object implements a
/// particular version of its `Interface`. For global objects, the maximum
/// version supported by the server is advertised with the global and the actual
/// version of the created protocol object is determined by the version argument
/// passed to `wl_registry.bind()`. For objects that are not globals, their
/// version is inferred from the object that created them.
///
/// In order to keep things sane, this has a few implications for interface
/// versions:
///
/// * The object creation hierarchy must be a tree. Otherwise, infering object
///   versions from the parent object becomes a much more difficult to properly
///   track.
/// * When the version of an `Interface` increases, so does the version of its
///   parent (recursively until you get to a global `Interface`)
/// * A global `Interface`'s version number acts like a counter for all of its
///   child `Interface`s. Whenever a child `Interface` gets modified, the global
///   parent's `Interface` version number also increases (see above). The child
///   `Interface` then takes on the same version number as the new version of
///   its parent global `Interface`.
///
/// To illustrate the above, consider the `wl_compositor` `Interface`. It has
/// two children, `wl_surface` and `wl_region`. As of wayland version 1.2,
/// `wl_surface` and `wl_compositor` are both at version 3. If something is
/// added to the `wl_region` interface, both `wl_region` and `wl_compositor`
/// will get bumpped to version 4. If, afterwards, `wl_surface` is changed, both
/// `wl_compositor` and `wl_surface` will be at version 5. In this way the
/// global `Interface` version is used as a sort of "counter" for all of its
/// child `Interface`s. This makes it very simple to know the version of the
/// child given the version of its parent. The child is at the highest possible
/// `Interface` version that is less than or equal to its parent's version.
///
/// It is worth noting a particular exception to the above versioning scheme.
/// The `wl_display` (and, by extension, `wl_registry`) `Interface` cannot
/// change because it is the core protocol object and its version is never
/// advertised nor is there a mechanism to request a different version.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct InterfaceVersion(pub u8);

impl fmt::Display for InterfaceVersion {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Name of a `Message`
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct MessageName(pub String);

impl fmt::Display for MessageName {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Name of an `Argument`
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct ArgumentName(pub String);

impl fmt::Display for ArgumentName {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Name of an `Enumeration`
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct EnumerationName(pub String);

impl fmt::Display for EnumerationName {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Name of an `Entry`
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct EntryName(pub String);

impl fmt::Display for EntryName {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// # Entry Value
///
/// The value of an `Entry`
///
/// The `Enumeration` attribute can be used on either `uint` or `int`
/// `Argument`s, however if the `Enumeration` is defined as a bitfield, it can
/// only be used on `uint` `Argument`s.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[must_use]
pub enum EntryValue {
    /// A value in a bitfield `Enumeration`
    BitfieldValue(usize),

    /// A value in a non-bitfield `Enumeration`
    EnumerantValue(isize),
}

impl fmt::Display for EntryValue {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        match self {
            Self::BitfieldValue(x) => write!(f, "{}", x),
            Self::EnumerantValue(x) => write!(f, "{}", x),
        }
    }
}

/// # Protocol
///
/// The Wayland protocol is an asynchronous object oriented protocol. All
/// requests are method invocations on some object. The requests include an
/// object ID that uniquely identifies an object on the server. Each object
/// implements an `Interface` and the requests include an opcode that identifies
/// which method in the interface to invoke
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[must_use]
pub struct Protocol {
    /// Name of the protocol.
    pub name: ProtocolName,

    /// Description text of the protocol.
    pub description: Option<String>,

    /// Description summary of the protocol.
    pub summary: Option<String>,

    /// Copyright notice.
    pub copyright: Option<Copyright>,

    /// List of interfaces.
    pub interfaces: Vec<Interface>,
}

/// # Interface
///
/// The Wayland protocol is message-based. A message sent by a client to the
/// server is called request. A message from the server to a client is called
/// event.
///
/// Additionally, the protocol can specify `Enumeration`s which associate names
/// to specific numeric enumeration values.
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[must_use]
pub struct Interface {
    /// Name of the `Interface`.
    pub name: InterfaceName,

    /// Description text of the `Interface`.
    pub description: Option<String>,

    /// Description summary of the `Interface`.
    pub summary: Option<String>,

    /// Version number of the `Interface`. See `InterfaceVersion` for details.
    pub version: InterfaceVersion,

    /// List of `Message`s the interface supports.
    pub messages: Vec<Message>,

    /// List of `Enumeration`s the interface supports.
    pub enumerations: Vec<Enumeration>,
}

/// # Message
///
/// The Wayland protocol is message-based. A message sent by a client to the
/// server is called request. A message from the server to a client is called
/// event. A message has a number of arguments, each of which has a certain type
/// (see `ArgumentType` for a list of argument types).
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[must_use]
pub struct Message {
    /// Name of the `Message`
    pub name: MessageName,

    /// Description text of the `Message`
    pub description: Option<String>,

    /// Description summary of the `Message`
    pub summary: Option<String>,

    /// `Message` Type (Request or Event)
    pub r#type: MessageType,

    /// `Message` arguments
    pub arguments: Vec<Argument>,
}

/// # Message Type
///
/// The Wayland protocol is message-based. A message sent by a client to the
/// server is called request. A message from the server to a client is called
/// event. A message has a number of arguments, each of which has a certain type
/// (see `ArgumentType` for a list of argument types).
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[must_use]
pub enum MessageType {
    /// A message sent by a client to the server.
    Request,

    /// A message from the server to a client.
    Event,
}

/// # Argument
///
/// A `Message` argument
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[must_use]
pub struct Argument {
    /// Name of the `Argument`
    pub name: ArgumentName,

    /// Description text of the `Argument`
    pub description: Option<String>,

    /// Description summary of the `Argument`
    pub summary: Option<String>,

    /// The type of the `Argument`
    pub r#type: ArgumentType,

    /// The `Interface` of the `Argument`
    pub interface: Option<InterfaceName>,

    /// Whether the `Argument` can be null
    pub allow_null: bool,
}

/// # Enumeration
///
/// The Wayland protocol can specify `Enumeration`s which associate names to
/// specific numeric enumeration values. These are primarily just descriptive in
/// nature: at the wire format level `Enumeration`s are just integers. But they
/// also serve a secondary purpose to enhance type safety or otherwise add
/// context for use in language bindings or other such code. This latter usage
/// is only supported so long as code written before these attributes were
/// introduced still works after; in other words, adding an enum should not
/// break API, otherwise it puts backwards compatibility at risk.
///
///
/// `Enumeration`s can be defined as just a set of integers, or as bitfields.
/// This is specified via the bitfield boolean attribute in the enum definition.
/// If this attribute is true, the enum is intended to be accessed primarily
/// using bitwise operations, for example when arbitrarily many choices of the
/// enum can be `OR`ed together; if it is false, or the attribute is omitted,
/// then the enum arguments are a just a sequence of numerical values.
#[derive(Debug, Clone, Default, PartialEq, Eq, Hash)]
#[must_use]
pub struct Enumeration {
    /// Name of the `Enumeration`
    pub name: EnumerationName,

    /// Description text of the `Enumeration`
    pub description: Option<String>,

    /// Description summary of the `Enumeration`
    pub summary: Option<String>,

    /// Bitfield attribute
    pub bitfield: bool,

    /// List of `Entry` in the `Enumeration`
    pub entries: Vec<Entry>,
}

/// # Entry
///
/// An entry in an `Enumeration`
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[must_use]
pub struct Entry {
    /// Name of the `Entry`
    pub name: EntryName,

    /// Description text of the `Entry`
    pub description: Option<String>,

    /// Description summary of the `Entry`
    pub summary: Option<String>,

    /// Value of the `Entry`
    pub value: EntryValue,
}

/// # Argument Type
///
/// The type of an `Argument`
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[must_use]
pub enum ArgumentType {
    /// The value is the 32-bit value of the signed int.
    SignedInteger,

    /// The value is the 32-bit value of the unsigned int.
    UnsignedInteger,

    /// Signed 24.8 decimal numbers. It is a signed decimal type which
    /// offers a sign bit, 23 bits of integer precision and 8 bits of
    /// decimal precision. This is exposed as an opaque struct with
    /// conversion helpers to and from double and int on the C API side.
    FixedPoint,

    /// Starts with an unsigned 32-bit length, followed by the string
    /// contents, including terminating null byte, then padding to a 32-bit
    /// boundary.
    String,

    /// 32-bit object ID.
    Object,

    /// The 32-bit object ID. On requests, the client decides the ID.
    /// The only events with new_id are advertisements of globals, and
    /// the server will use IDs below 0x10000.
    NewId,

    /// Starts with 32-bit array size in bytes, followed by the array
    /// contents verbatim, and finally padding to a 32-bit boundary.
    Array,

    /// The file descriptor is not stored in the message buffer, but in
    /// the ancillary data of the UNIX domain socket message (msg_control).
    FileDescriptor,
}
